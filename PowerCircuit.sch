EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR0143
U 1 1 60095EFE
P 7350 4975
F 0 "#PWR0143" H 7350 4825 50  0001 C CNN
F 1 "+5V" V 7365 5103 50  0000 L CNN
F 2 "" H 7350 4975 50  0001 C CNN
F 3 "" H 7350 4975 50  0001 C CNN
	1    7350 4975
	0    -1   -1   0   
$EndComp
$Comp
L Device:D_Schottky_Small D5
U 1 1 60095F04
P 7350 5075
F 0 "D5" V 7396 5005 50  0000 R CNN
F 1 "B5189W" V 7305 5005 50  0000 R CNN
F 2 "Diode_SMD:D_SOD-123" V 7350 5075 50  0001 C CNN
F 3 "~" V 7350 5075 50  0001 C CNN
	1    7350 5075
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7350 5425 7550 5425
Connection ~ 7350 5425
Wire Wire Line
	7350 5425 7350 5175
$Comp
L Device:C_Small C7
U 1 1 60095F14
P 3425 3450
F 0 "C7" V 3196 3450 50  0000 C CNN
F 1 "10n" V 3287 3450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3425 3450 50  0001 C CNN
F 3 "~" H 3425 3450 50  0001 C CNN
	1    3425 3450
	0    1    1    0   
$EndComp
Text GLabel 3325 3450 0    50   Input ~ 0
BUCK_BST
Text GLabel 3525 3450 2    50   Input ~ 0
BUCK_SW
$Comp
L MotorController-rescue:MP2359-MP2359-SensorBoard-rescue REG1
U 1 1 60095F1C
P 3450 5225
F 0 "REG1" H 3400 5550 50  0000 C CNN
F 1 "MP2359" H 3400 5459 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 3450 5225 50  0001 C CNN
F 3 "" H 3450 5225 50  0001 C CNN
	1    3450 5225
	1    0    0    -1  
$EndComp
Text GLabel 3050 5125 0    50   Input ~ 0
BUCK_IN
Text GLabel 3050 5525 0    50   Input ~ 0
BUCK_EN
Text GLabel 3750 5125 2    50   Input ~ 0
BUCK_BST
Text GLabel 3750 5325 2    50   Input ~ 0
BUCK_SW
Text GLabel 3750 5525 2    50   Input ~ 0
BUCK_FB
$Comp
L power:GND #PWR0145
U 1 1 60095F27
P 3450 5775
F 0 "#PWR0145" H 3450 5525 50  0001 C CNN
F 1 "GND" H 3455 5602 50  0000 C CNN
F 2 "" H 3450 5775 50  0001 C CNN
F 3 "" H 3450 5775 50  0001 C CNN
	1    3450 5775
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 5675 3450 5775
Text GLabel 5875 3550 0    50   Input ~ 0
BUCK_SW
Text GLabel 4900 3550 2    50   Input ~ 0
BUCK_FB
Wire Wire Line
	4500 3550 4500 3600
Connection ~ 4500 3550
Wire Wire Line
	4500 3550 4900 3550
$Comp
L power:GND #PWR0146
U 1 1 60095F33
P 4500 4250
F 0 "#PWR0146" H 4500 4000 50  0001 C CNN
F 1 "GND" H 4505 4077 50  0000 C CNN
F 2 "" H 4500 4250 50  0001 C CNN
F 3 "" H 4500 4250 50  0001 C CNN
	1    4500 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4150 4500 4250
Wire Wire Line
	4500 3800 4500 3950
Wire Wire Line
	4500 3500 4500 3550
$Comp
L Device:R_Small R22
U 1 1 60095F3C
P 4500 4050
F 0 "R22" H 4559 4096 50  0000 L CNN
F 1 "270k" H 4559 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4500 4050 50  0001 C CNN
F 3 "~" H 4500 4050 50  0001 C CNN
	1    4500 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R21
U 1 1 60095F42
P 4500 3700
F 0 "R21" H 4559 3746 50  0000 L CNN
F 1 "15k" H 4559 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4500 3700 50  0001 C CNN
F 3 "~" H 4500 3700 50  0001 C CNN
	1    4500 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3150 4500 3300
$Comp
L Device:R_Small R20
U 1 1 60095F49
P 4500 3400
F 0 "R20" H 4559 3446 50  0000 L CNN
F 1 "47k" H 4559 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4500 3400 50  0001 C CNN
F 3 "~" H 4500 3400 50  0001 C CNN
	1    4500 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0147
U 1 1 60095F4F
P 4500 3150
F 0 "#PWR0147" H 4500 3000 50  0001 C CNN
F 1 "+3.3V" H 4515 3323 50  0000 C CNN
F 2 "" H 4500 3150 50  0001 C CNN
F 3 "" H 4500 3150 50  0001 C CNN
	1    4500 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0148
U 1 1 60095F55
P 7125 4000
F 0 "#PWR0148" H 7125 3750 50  0001 C CNN
F 1 "GND" H 7130 3827 50  0000 C CNN
F 2 "" H 7125 4000 50  0001 C CNN
F 3 "" H 7125 4000 50  0001 C CNN
	1    7125 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0149
U 1 1 60095F5B
P 6825 4000
F 0 "#PWR0149" H 6825 3750 50  0001 C CNN
F 1 "GND" H 6830 3827 50  0000 C CNN
F 2 "" H 6825 4000 50  0001 C CNN
F 3 "" H 6825 4000 50  0001 C CNN
	1    6825 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7125 3850 7125 4000
Wire Wire Line
	6825 3850 6825 4000
$Comp
L Device:C_Small C9
U 1 1 60095F66
P 7125 3750
F 0 "C9" H 7217 3796 50  0000 L CNN
F 1 "10u" H 7217 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 7125 3750 50  0001 C CNN
F 3 "~" H 7125 3750 50  0001 C CNN
	1    7125 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 60095F6C
P 6825 3750
F 0 "C8" H 6917 3796 50  0000 L CNN
F 1 "10u" H 6917 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 6825 3750 50  0001 C CNN
F 3 "~" H 6825 3750 50  0001 C CNN
	1    6825 3750
	1    0    0    -1  
$EndComp
Connection ~ 6075 3550
Wire Wire Line
	6075 3550 6475 3550
$Comp
L Device:L_Small L2
U 1 1 60095F76
P 6575 3550
F 0 "L2" V 6760 3550 50  0000 C CNN
F 1 "10u" V 6669 3550 50  0000 C CNN
F 2 "Inductor_SMD:L_Sunlord_MWSA0518_5.4x5.2mm" H 6575 3550 50  0001 C CNN
F 3 "~" H 6575 3550 50  0001 C CNN
	1    6575 3550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0150
U 1 1 60095F7C
P 6075 3900
F 0 "#PWR0150" H 6075 3650 50  0001 C CNN
F 1 "GND" H 6080 3727 50  0000 C CNN
F 2 "" H 6075 3900 50  0001 C CNN
F 3 "" H 6075 3900 50  0001 C CNN
	1    6075 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6075 3750 6075 3900
Wire Wire Line
	5875 3550 6075 3550
$Comp
L Device:D_Schottky_Small D4
U 1 1 60095F84
P 6075 3650
F 0 "D4" V 6029 3720 50  0000 L CNN
F 1 "B5189W" V 6120 3720 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" V 6075 3650 50  0001 C CNN
F 3 "~" V 6075 3650 50  0001 C CNN
	1    6075 3650
	0    1    1    0   
$EndComp
Text GLabel 5250 5000 0    50   Input ~ 0
BUCK_IN
Text GLabel 5550 5350 2    50   Input ~ 0
BUCK_EN
Wire Wire Line
	5350 5350 5350 5450
Connection ~ 5350 5350
Wire Wire Line
	5350 5350 5550 5350
$Comp
L power:GND #PWR0151
U 1 1 60095F8F
P 5350 5700
F 0 "#PWR0151" H 5350 5450 50  0001 C CNN
F 1 "GND" H 5355 5527 50  0000 C CNN
F 2 "" H 5350 5700 50  0001 C CNN
F 3 "" H 5350 5700 50  0001 C CNN
	1    5350 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 5000 5250 5000
Wire Wire Line
	5350 5050 5350 5000
Wire Wire Line
	5350 5650 5350 5700
Wire Wire Line
	5350 5250 5350 5350
$Comp
L Device:R_Small R24
U 1 1 60095F99
P 5350 5550
F 0 "R24" H 5409 5596 50  0000 L CNN
F 1 "68k" H 5409 5505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5350 5550 50  0001 C CNN
F 3 "~" H 5350 5550 50  0001 C CNN
	1    5350 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R23
U 1 1 60095F9F
P 5350 5150
F 0 "R23" H 5409 5196 50  0000 L CNN
F 1 "100k" H 5409 5105 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5350 5150 50  0001 C CNN
F 3 "~" H 5350 5150 50  0001 C CNN
	1    5350 5150
	1    0    0    -1  
$EndComp
Text GLabel 7900 5425 2    50   Input ~ 0
BUCK_IN
$Comp
L power:GND #PWR0152
U 1 1 60095FA6
P 7900 5875
F 0 "#PWR0152" H 7900 5625 50  0001 C CNN
F 1 "GND" H 7905 5702 50  0000 C CNN
F 2 "" H 7900 5875 50  0001 C CNN
F 3 "" H 7900 5875 50  0001 C CNN
	1    7900 5875
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 5775 7900 5875
$Comp
L Device:C_Small C10
U 1 1 60095FAD
P 7900 5675
F 0 "C10" H 7992 5721 50  0000 L CNN
F 1 "10u" H 7992 5630 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 7900 5675 50  0001 C CNN
F 3 "~" H 7900 5675 50  0001 C CNN
	1    7900 5675
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 5425 7900 5575
Wire Wire Line
	7750 5425 7900 5425
Wire Wire Line
	7250 5425 7350 5425
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 60095FB6
P 7650 5425
F 0 "FB1" V 7413 5425 50  0000 C CNN
F 1 "600 Ohms 600MHz" V 7504 5425 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 7580 5425 50  0001 C CNN
F 3 "~" H 7650 5425 50  0001 C CNN
	1    7650 5425
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0153
U 1 1 60095FBC
P 7050 5825
F 0 "#PWR0153" H 7050 5575 50  0001 C CNN
F 1 "GND" H 7055 5652 50  0000 C CNN
F 2 "" H 7050 5825 50  0001 C CNN
F 3 "" H 7050 5825 50  0001 C CNN
	1    7050 5825
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 5725 7050 5825
Wire Wire Line
	6750 5425 6850 5425
$Comp
L Transistor_FET:AO3401A Q3
U 1 1 60095FC4
P 7050 5525
F 0 "Q3" V 7392 5525 50  0000 C CNN
F 1 "AO3401A" V 7301 5525 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7250 5450 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3401A.pdf" H 7050 5525 50  0001 L CNN
	1    7050 5525
	0    -1   -1   0   
$EndComp
$Comp
L Device:Polyfuse_Small F1
U 1 1 60095FCC
P 6650 5425
F 0 "F1" V 6445 5425 50  0000 C CNN
F 1 "500mA" V 6536 5425 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric" H 6700 5225 50  0001 L CNN
F 3 "~" H 6650 5425 50  0001 C CNN
	1    6650 5425
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 5425 6550 5425
Wire Wire Line
	6350 5225 6350 5425
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5FE7CF9F
P 6350 5425
F 0 "#FLG0103" H 6350 5500 50  0001 C CNN
F 1 "PWR_FLAG" H 6350 5598 50  0000 C CNN
F 2 "" H 6350 5425 50  0001 C CNN
F 3 "~" H 6350 5425 50  0001 C CNN
	1    6350 5425
	-1   0    0    1   
$EndComp
Connection ~ 6350 5425
Wire Wire Line
	7125 3550 7125 3650
$Comp
L power:+3.3V #PWR0144
U 1 1 60095F0E
P 7125 3550
F 0 "#PWR0144" H 7125 3400 50  0001 C CNN
F 1 "+3.3V" H 7140 3723 50  0000 C CNN
F 2 "" H 7125 3550 50  0001 C CNN
F 3 "" H 7125 3550 50  0001 C CNN
	1    7125 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6675 3550 6825 3550
Wire Wire Line
	6825 3550 6825 3650
Connection ~ 6825 3550
Wire Wire Line
	6825 3550 7125 3550
$Comp
L power:+5V #PWR0102
U 1 1 600C7EB3
P 6350 5225
F 0 "#PWR0102" H 6350 5075 50  0001 C CNN
F 1 "+5V" V 6365 5353 50  0000 L CNN
F 2 "" H 6350 5225 50  0001 C CNN
F 3 "" H 6350 5225 50  0001 C CNN
	1    6350 5225
	1    0    0    -1  
$EndComp
$EndSCHEMATC
