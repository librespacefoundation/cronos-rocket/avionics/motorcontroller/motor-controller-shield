EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small R?
U 1 1 60731427
P 4019 4264
AR Path="/60731427" Ref="R?"  Part="1" 
AR Path="/606B9024/60731427" Ref="R?"  Part="1" 
AR Path="/6088E838/60731427" Ref="R?"  Part="1" 
AR Path="/608B4A3E/60731427" Ref="R?"  Part="1" 
AR Path="/607250D5/60731427" Ref="R?"  Part="1" 
AR Path="/6072C147/60731427" Ref="R17"  Part="1" 
F 0 "R17" V 3823 4264 50  0000 C CNN
F 1 "1K" V 3914 4264 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4019 4264 50  0001 C CNN
F 3 "~" H 4019 4264 50  0001 C CNN
	1    4019 4264
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 6073142D
P 7045 3475
AR Path="/6073142D" Ref="R?"  Part="1" 
AR Path="/606B9024/6073142D" Ref="R?"  Part="1" 
AR Path="/6088E838/6073142D" Ref="R?"  Part="1" 
AR Path="/608B4A3E/6073142D" Ref="R?"  Part="1" 
AR Path="/607250D5/6073142D" Ref="R?"  Part="1" 
AR Path="/6072C147/6073142D" Ref="R31"  Part="1" 
F 0 "R31" V 6849 3475 50  0000 C CNN
F 1 "1K" V 6940 3475 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7045 3475 50  0001 C CNN
F 3 "~" H 7045 3475 50  0001 C CNN
	1    7045 3475
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 60731433
P 7045 4301
AR Path="/60731433" Ref="R?"  Part="1" 
AR Path="/606B9024/60731433" Ref="R?"  Part="1" 
AR Path="/6088E838/60731433" Ref="R?"  Part="1" 
AR Path="/608B4A3E/60731433" Ref="R?"  Part="1" 
AR Path="/607250D5/60731433" Ref="R?"  Part="1" 
AR Path="/6072C147/60731433" Ref="R32"  Part="1" 
F 0 "R32" V 6849 4301 50  0000 C CNN
F 1 "1K" V 6940 4301 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7045 4301 50  0001 C CNN
F 3 "~" H 7045 4301 50  0001 C CNN
	1    7045 4301
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60731439
P 5541 4609
AR Path="/60731439" Ref="#PWR?"  Part="1" 
AR Path="/606B9024/60731439" Ref="#PWR?"  Part="1" 
AR Path="/6088E838/60731439" Ref="#PWR?"  Part="1" 
AR Path="/608B4A3E/60731439" Ref="#PWR?"  Part="1" 
AR Path="/607250D5/60731439" Ref="#PWR?"  Part="1" 
AR Path="/6072C147/60731439" Ref="#PWR0137"  Part="1" 
F 0 "#PWR0137" H 5541 4359 50  0001 C CNN
F 1 "GND" H 5546 4436 50  0000 C CNN
F 2 "" H 5541 4609 50  0001 C CNN
F 3 "" H 5541 4609 50  0001 C CNN
	1    5541 4609
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 6073143F
P 4006 3513
AR Path="/6073143F" Ref="R?"  Part="1" 
AR Path="/606B9024/6073143F" Ref="R?"  Part="1" 
AR Path="/6088E838/6073143F" Ref="R?"  Part="1" 
AR Path="/608B4A3E/6073143F" Ref="R?"  Part="1" 
AR Path="/607250D5/6073143F" Ref="R?"  Part="1" 
AR Path="/6072C147/6073143F" Ref="R16"  Part="1" 
F 0 "R16" V 3810 3513 50  0000 C CNN
F 1 "1K" V 3901 3513 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4006 3513 50  0001 C CNN
F 3 "~" H 4006 3513 50  0001 C CNN
	1    4006 3513
	0    1    1    0   
$EndComp
Wire Wire Line
	4329 3513 4106 3513
Wire Wire Line
	4329 4264 4119 4264
Wire Wire Line
	6389 3675 6389 3880
Wire Wire Line
	6689 3475 6945 3475
Wire Wire Line
	6689 4301 6945 4301
Wire Wire Line
	5541 4501 5541 4609
Connection ~ 5541 4501
Wire Wire Line
	5495 3198 5495 3275
Connection ~ 5495 3275
Connection ~ 4629 3881
Wire Wire Line
	4629 3881 4629 4064
Connection ~ 6389 3880
Wire Wire Line
	6389 3880 6389 4101
Wire Wire Line
	4629 3713 4629 3881
Wire Wire Line
	4629 4501 4629 4464
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 6073147F
P 5494 3680
AR Path="/606B9024/6073147F" Ref="J?"  Part="1" 
AR Path="/6088E838/6073147F" Ref="J?"  Part="1" 
AR Path="/608B4A3E/6073147F" Ref="J?"  Part="1" 
AR Path="/607250D5/6073147F" Ref="J?"  Part="1" 
AR Path="/6072C147/6073147F" Ref="J4"  Part="1" 
F 0 "J4" V 5556 3492 50  0000 R CNN
F 1 "Conn_01x02_Male" V 5647 3492 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 5494 3680 50  0001 C CNN
F 3 "~" H 5494 3680 50  0001 C CNN
	1    5494 3680
	0    -1   1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60731485
P 5495 3198
AR Path="/606B9024/60731485" Ref="#PWR?"  Part="1" 
AR Path="/608B4A3E/60731485" Ref="#PWR?"  Part="1" 
AR Path="/607250D5/60731485" Ref="#PWR?"  Part="1" 
AR Path="/6072C147/60731485" Ref="#PWR0140"  Part="1" 
F 0 "#PWR0140" H 5495 3048 50  0001 C CNN
F 1 "+5V" H 5510 3371 50  0000 C CNN
F 2 "" H 5495 3198 50  0001 C CNN
F 3 "" H 5495 3198 50  0001 C CNN
	1    5495 3198
	1    0    0    -1  
$EndComp
Wire Wire Line
	4629 3275 4629 3313
$Comp
L Transistor_FET:C2M0025120D Q?
U 1 1 6073148C
P 4529 4264
AR Path="/608B4A3E/6073148C" Ref="Q?"  Part="1" 
AR Path="/606B9024/6073148C" Ref="Q?"  Part="1" 
AR Path="/607250D5/6073148C" Ref="Q?"  Part="1" 
AR Path="/6072C147/6073148C" Ref="Q11"  Part="1" 
F 0 "Q11" H 4733 4310 50  0000 L CNN
F 1 "C2M0025120D" H 4733 4219 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-247-3_Vertical" H 4529 4264 50  0001 C CIN
F 3 "https://www.wolfspeed.com/media/downloads/161/C2M0025120D.pdf" H 4529 4264 50  0001 L CNN
	1    4529 4264
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:C2M0025120D Q?
U 1 1 60731492
P 6489 4301
AR Path="/608B4A3E/60731492" Ref="Q?"  Part="1" 
AR Path="/606B9024/60731492" Ref="Q?"  Part="1" 
AR Path="/607250D5/60731492" Ref="Q?"  Part="1" 
AR Path="/6072C147/60731492" Ref="Q13"  Part="1" 
F 0 "Q13" H 6694 4347 50  0000 L CNN
F 1 "C2M0025120D" H 6694 4256 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-247-3_Vertical" H 6489 4301 50  0001 C CIN
F 3 "https://www.wolfspeed.com/media/downloads/161/C2M0025120D.pdf" H 6489 4301 50  0001 L CNN
	1    6489 4301
	-1   0    0    -1  
$EndComp
$Comp
L Transistor_FET:FQP27P06 Q?
U 1 1 60731498
P 6489 3475
AR Path="/608B4A3E/60731498" Ref="Q?"  Part="1" 
AR Path="/606B9024/60731498" Ref="Q?"  Part="1" 
AR Path="/607250D5/60731498" Ref="Q?"  Part="1" 
AR Path="/6072C147/60731498" Ref="Q12"  Part="1" 
F 0 "Q12" H 6694 3429 50  0000 L CNN
F 1 "FQP27P06" H 6694 3520 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6689 3400 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/FQP27P06-D.PDF" H 6489 3475 50  0001 L CNN
	1    6489 3475
	-1   0    0    1   
$EndComp
$Comp
L Transistor_FET:FQP27P06 Q?
U 1 1 6073149F
P 4529 3513
AR Path="/608B4A3E/6073149F" Ref="Q?"  Part="1" 
AR Path="/606B9024/6073149F" Ref="Q?"  Part="1" 
AR Path="/607250D5/6073149F" Ref="Q?"  Part="1" 
AR Path="/6072C147/6073149F" Ref="Q10"  Part="1" 
F 0 "Q10" H 4733 3467 50  0000 L CNN
F 1 "FQP27P06" H 4733 3558 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 4729 3438 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/FQP27P06-D.PDF" H 4529 3513 50  0001 L CNN
	1    4529 3513
	1    0    0    1   
$EndComp
Wire Wire Line
	3336 3513 3906 3513
Wire Wire Line
	7145 3475 7806 3475
Wire Wire Line
	3336 4264 3919 4264
Wire Wire Line
	3336 3513 3336 3839
Wire Wire Line
	3336 3839 3009 3839
Wire Wire Line
	3009 3839 3009 3838
Connection ~ 3336 3839
Wire Wire Line
	3336 3839 3336 4264
Text HLabel 3009 3838 0    50   Input ~ 0
LEFT1
Wire Wire Line
	7806 3475 7806 3862
Wire Wire Line
	7145 4301 7806 4301
Wire Wire Line
	7806 3862 7971 3862
Connection ~ 7806 3862
Wire Wire Line
	7806 3862 7806 4301
Text HLabel 7971 3862 2    50   Input ~ 0
RIGHT1
Wire Wire Line
	4629 3275 5495 3275
Wire Wire Line
	5495 3275 6389 3275
Wire Wire Line
	5594 3880 6389 3880
Wire Wire Line
	5541 4501 6389 4501
Wire Wire Line
	4629 4501 5541 4501
Wire Wire Line
	5494 3881 5494 3880
Wire Wire Line
	4629 3881 5494 3881
$EndSCHEMATC
