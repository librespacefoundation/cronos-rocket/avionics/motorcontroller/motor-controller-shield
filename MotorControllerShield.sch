EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	4181 7798 4177 7798
Wire Notes Line
	474  2847 474  2842
Wire Notes Line
	7881 476  7891 476 
$Comp
L Graphic:Logo_Open_Hardware_Small #LOGO1
U 1 1 5DDFA556
P 7294 6866
F 0 "#LOGO1" H 7294 7141 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 7294 6641 50  0001 C CNN
F 2 "Symbol:OSHW-Logo2_9.8x8mm_SilkScreen" H 7294 6866 50  0001 C CNN
F 3 "~" H 7294 6866 50  0001 C CNN
	1    7294 6866
	1    0    0    -1  
$EndComp
Text GLabel 7575 1125 0    50   Input ~ 0
INJ_MSP300_+
Text GLabel 7575 1225 0    50   Input ~ 0
INJ_MSP300_-
Text GLabel 7575 2025 0    50   Input ~ 0
Engine_LC1_-
Text GLabel 7575 1925 0    50   Input ~ 0
Engine_LC1_+
Text GLabel 7575 1625 0    50   Input ~ 0
RT_MSP300_-
Text GLabel 7575 1525 0    50   Input ~ 0
RT_MSP300_+
Text GLabel 7575 1425 0    50   Input ~ 0
CC_MCP300_-
Text GLabel 7575 1325 0    50   Input ~ 0
CC_MCP300_+
Text GLabel 7575 2325 0    50   Input ~ 0
Engine_LC2_3_+
Text GLabel 7575 2425 0    50   Input ~ 0
Engine_LC2_3_-
Text GLabel 7575 2125 0    50   Input ~ 0
Engine_LC2_+
Text GLabel 7575 2225 0    50   Input ~ 0
Engine_LC2_-
Wire Wire Line
	1950 4500 1800 4500
Wire Wire Line
	1950 4375 1800 4375
Wire Wire Line
	2975 4425 2825 4425
Text GLabel 7575 2975 0    50   Input ~ 0
THRUST_POT
Text GLabel 7575 2875 0    50   Input ~ 0
PURGE_POT
$Sheet
S 1950 4275 875  300 
U 60E1209C
F0 "sheet60E12097" 50
F1 "INA.sch" 50
F2 "INPUT_+" I L 1950 4375 50 
F3 "INPUT_-" I L 1950 4500 50 
F4 "OUTPUT" O R 2825 4425 50 
$EndSheet
Text GLabel 1825 1175 0    50   Input ~ 0
PURGE_LEFT
Text GLabel 1825 1075 0    50   Input ~ 0
PURGE_RIGHT
Text GLabel 1825 1825 0    50   Input ~ 0
THRUST_RIGHT
Text GLabel 1825 1925 0    50   Input ~ 0
THRUST_LEFT
Text GLabel 1825 2075 0    50   Input ~ 0
QR_RIGHT
Text GLabel 2200 3350 0    50   Input ~ 0
Pyro2
Text GLabel 3100 3300 0    50   Input ~ 0
Pyro3
Text GLabel 3972 3324 0    50   Input ~ 0
Pyro4
$Sheet
S 1300 3050 550  550 
U 60551091
F0 "Pyro1" 50
F1 "Pyro1.sch" 50
F2 "SIGNAL" I L 1300 3350 50 
$EndSheet
$Sheet
S 3972 3049 528  551 
U 60FA7E41
F0 "Pyro4" 50
F1 "Pyro1.sch" 50
F2 "SIGNAL" I L 3972 3324 50 
$EndSheet
Text GLabel 1300 3350 0    50   Input ~ 0
Pyro1
$Sheet
S 3100 3050 550  550 
U 60FA1AA9
F0 "Pyro3" 50
F1 "Pyro1.sch" 50
F2 "SIGNAL" I L 3100 3300 50 
$EndSheet
$Sheet
S 2200 3050 550  550 
U 60F9B7F6
F0 "Pyro2" 50
F1 "Pyro1.sch" 50
F2 "SIGNAL" I L 2200 3350 50 
$EndSheet
$Sheet
S 1825 975  825  1500
U 60D554BE
F0 "DC motor drivers" 50
F1 "DC motor drivers.sch" 50
F2 "PURGE_RIGHT" I L 1825 1075 50 
F3 "PURGE_LEFT" I L 1825 1175 50 
F4 "THRUST_RIGHT" I L 1825 1825 50 
F5 "THRUST_LEFT" I L 1825 1925 50 
F6 "QR_RIGHT" I L 1825 2075 50 
F7 "GR_LEFT" I L 1825 2175 50 
F8 "CO2_1_RIGHT" I L 1825 1325 50 
F9 "CO2_1_LEFT" I L 1825 1425 50 
F10 "CO2_2_RIGHT" I L 1825 1575 50 
F11 "CO2_2_LEFT" I L 1825 1675 50 
F12 "3WAY_RIGHT" I L 1825 2325 50 
F13 "3WAY_LEFT" I L 1825 2425 50 
$EndSheet
$Comp
L Connector_Generic:Conn_01x26 J3
U 1 1 60DAC8E8
P 9900 2400
F 0 "J3" H 9980 2392 50  0000 L CNN
F 1 "Shield connector" H 9980 2301 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_2x13_P2.00mm_Vertical" H 9900 2400 50  0001 C CNN
F 3 "~" H 9900 2400 50  0001 C CNN
	1    9900 2400
	1    0    0    -1  
$EndComp
Text GLabel 9700 1200 0    50   Input ~ 0
INJ_MSP300
Text GLabel 9700 1300 0    50   Input ~ 0
RT_MSP300
Text GLabel 9700 1400 0    50   Input ~ 0
CC_MCP300
Text GLabel 9700 1500 0    50   Input ~ 0
PURGE_POT
Text GLabel 9700 1600 0    50   Input ~ 0
3WAY_POT
Text GLabel 9700 1700 0    50   Input ~ 0
THRUST_POT
Text GLabel 9700 1900 0    50   Input ~ 0
PURGE_LEFT
Text GLabel 9700 1800 0    50   Input ~ 0
3WAY_RIGHT
Text GLabel 9700 2000 0    50   Input ~ 0
QR_LEFT
Text GLabel 9700 2100 0    50   Input ~ 0
QR_RIGHT
Text GLabel 9700 2300 0    50   Input ~ 0
THRUST_RIGHT
Text GLabel 9700 2200 0    50   Input ~ 0
Engine_LC1
Text GLabel 9700 2500 0    50   Input ~ 0
Engine_LC2
Text GLabel 9700 2400 0    50   Input ~ 0
Engine_LC3
Text GLabel 9700 2700 0    50   Input ~ 0
CO2_1_LEFT
Text GLabel 9700 2600 0    50   Input ~ 0
CO2_1_RIGHT
Text GLabel 9700 2900 0    50   Input ~ 0
Pyro4
Text GLabel 9700 2800 0    50   Input ~ 0
Pyro3
Text GLabel 9700 3100 0    50   Input ~ 0
Pyro2
Text GLabel 9700 3000 0    50   Input ~ 0
Pyro1
Text GLabel 9700 3300 0    50   Input ~ 0
THRUST_LEFT
Text GLabel 9700 3200 0    50   Input ~ 0
CO2_2_LEFT
Text GLabel 9700 3500 0    50   Input ~ 0
PURGE_RIGHT
Text GLabel 9700 3400 0    50   Input ~ 0
3WAY_LEFT
Text GLabel 9700 3600 0    50   Input ~ 0
RunTank_LC
Text GLabel 9700 3700 0    50   Input ~ 0
CO2_2_RIGHT
Text GLabel 7575 3075 0    50   Input ~ 0
3WAY_POT
Text GLabel 1800 4500 0    50   Input ~ 0
Engine_LC1_-
Text GLabel 1800 4375 0    50   Input ~ 0
Engine_LC1_+
Text GLabel 1825 1325 0    50   Input ~ 0
CO2_1_RIGHT
Text GLabel 1825 1425 0    50   Input ~ 0
CO2_1_LEFT
Text GLabel 1825 1675 0    50   Input ~ 0
CO2_2_LEFT
Text GLabel 1825 2425 0    50   Input ~ 0
3WAY_LEFT
Text GLabel 1825 2325 0    50   Input ~ 0
3WAY_RIGHT
Text GLabel 2975 4425 2    50   Input ~ 0
Engine_LC1
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 60E0A698
P 7775 2225
F 0 "J2" H 7855 2217 50  0000 L CNN
F 1 "Conn_01x08" H 7855 2126 50  0000 L CNN
F 2 "Connector_Molex:Molex_PicoBlade_53398-0871_1x08-1MP_P1.25mm_Vertical" H 7775 2225 50  0001 C CNN
F 3 "~" H 7775 2225 50  0001 C CNN
	1    7775 2225
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J4
U 1 1 60DD2096
P 7775 1325
F 0 "J4" H 7855 1317 50  0000 L CNN
F 1 "PicoBlade_01x06" H 7855 1226 50  0000 L CNN
F 2 "Connector_Molex:Molex_PicoBlade_53398-0671_1x06-1MP_P1.25mm_Vertical" H 7775 1325 50  0001 C CNN
F 3 "~" H 7775 1325 50  0001 C CNN
	1    7775 1325
	1    0    0    -1  
$EndComp
Text GLabel 1825 2175 0    50   Input ~ 0
QR_LEFT
Text GLabel 1825 1575 0    50   Input ~ 0
CO2_2_RIGHT
Text GLabel 7575 2525 0    50   Input ~ 0
RunTank_LC_+
Text GLabel 7575 2625 0    50   Input ~ 0
RunTank_LC_-
$Comp
L lsf-kicad:PQ9-Connector PQ1
U 1 1 60EA0D50
P 9875 4650
F 0 "PQ1" H 10223 4618 50  0000 L CNN
F 1 "PQ9-Connector" H 10223 4527 50  0000 L CNN
F 2 "lsf-kicad-lib:PQ9" H 10275 4150 50  0001 C CNN
F 3 "https://libre.space/pq9ish" H 10375 4850 50  0001 C CNN
	1    9875 4650
	1    0    0    -1  
$EndComp
NoConn ~ 9475 4250
NoConn ~ 9475 4350
NoConn ~ 9475 4450
NoConn ~ 9475 4650
NoConn ~ 9475 4750
$Comp
L power:+3V3 #PWR0125
U 1 1 60EA689D
P 9275 4525
F 0 "#PWR0125" H 9275 4375 50  0001 C CNN
F 1 "+3V3" H 9290 4698 50  0000 C CNN
F 2 "" H 9275 4525 50  0001 C CNN
F 3 "" H 9275 4525 50  0001 C CNN
	1    9275 4525
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0126
U 1 1 60EA7580
P 9275 4825
F 0 "#PWR0126" H 9275 4675 50  0001 C CNN
F 1 "+5V" H 9290 4998 50  0000 C CNN
F 2 "" H 9275 4825 50  0001 C CNN
F 3 "" H 9275 4825 50  0001 C CNN
	1    9275 4825
	1    0    0    -1  
$EndComp
Wire Wire Line
	9475 4550 9275 4550
Wire Wire Line
	9275 4550 9275 4525
Wire Wire Line
	9475 4850 9275 4850
Wire Wire Line
	9275 4850 9275 4825
$Comp
L power:GND #PWR0127
U 1 1 60EAA0C6
P 9875 5250
F 0 "#PWR0127" H 9875 5000 50  0001 C CNN
F 1 "GND" H 9880 5077 50  0000 C CNN
F 2 "" H 9875 5250 50  0001 C CNN
F 3 "" H 9875 5250 50  0001 C CNN
	1    9875 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 60EAD834
P 10075 5875
F 0 "J5" H 10155 5867 50  0000 L CNN
F 1 "Conn_01x02 (battery)" H 10155 5776 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 10075 5875 50  0001 C CNN
F 3 "~" H 10075 5875 50  0001 C CNN
	1    10075 5875
	1    0    0    -1  
$EndComp
$Comp
L power:+10V #PWR?
U 1 1 60EAEAE3
P 9800 5800
AR Path="/60551091/60EAEAE3" Ref="#PWR?"  Part="1" 
AR Path="/6055E6DD/60EAEAE3" Ref="#PWR?"  Part="1" 
AR Path="/60F9B7F6/60EAEAE3" Ref="#PWR?"  Part="1" 
AR Path="/60FA1AA9/60EAEAE3" Ref="#PWR?"  Part="1" 
AR Path="/60FA7E41/60EAEAE3" Ref="#PWR?"  Part="1" 
AR Path="/60EAEAE3" Ref="#PWR0128"  Part="1" 
F 0 "#PWR0128" H 9800 5650 50  0001 C CNN
F 1 "+10V" H 9815 5973 50  0000 C CNN
F 2 "" H 9800 5800 50  0001 C CNN
F 3 "" H 9800 5800 50  0001 C CNN
	1    9800 5800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 60EAF07C
P 9800 6050
F 0 "#PWR0129" H 9800 5800 50  0001 C CNN
F 1 "GND" H 9805 5877 50  0000 C CNN
F 2 "" H 9800 6050 50  0001 C CNN
F 3 "" H 9800 6050 50  0001 C CNN
	1    9800 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9875 5875 9800 5875
Wire Wire Line
	9800 5875 9800 5800
Wire Wire Line
	9875 5975 9800 5975
Wire Wire Line
	9800 5975 9800 6050
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60EB0DA0
P 9700 5875
F 0 "#FLG0101" H 9700 5950 50  0001 C CNN
F 1 "PWR_FLAG" V 9700 6002 50  0000 L CNN
F 2 "" H 9700 5875 50  0001 C CNN
F 3 "~" H 9700 5875 50  0001 C CNN
	1    9700 5875
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9700 5875 9800 5875
Connection ~ 9800 5875
Text GLabel 2975 6100 2    50   Input ~ 0
RunTank_LC
Text GLabel 2975 5525 2    50   Input ~ 0
Engine_LC3
Text GLabel 2975 4975 2    50   Input ~ 0
Engine_LC2
Text GLabel 1800 5050 0    50   Input ~ 0
Engine_LC2_-
Text GLabel 1800 4925 0    50   Input ~ 0
Engine_LC2_+
Text GLabel 1800 5600 0    50   Input ~ 0
Engine_LC2_3_-
Text GLabel 1800 5475 0    50   Input ~ 0
Engine_LC2_3_+
Text GLabel 1800 6175 0    50   Input ~ 0
RunTank_LC_-
Text GLabel 1800 6050 0    50   Input ~ 0
RunTank_LC_+
$Sheet
S 1950 5375 875  300 
U 60E3B510
F0 "sheet60E3B50B" 50
F1 "INA.sch" 50
F2 "INPUT_+" I L 1950 5475 50 
F3 "INPUT_-" I L 1950 5600 50 
F4 "OUTPUT" O R 2825 5525 50 
$EndSheet
$Sheet
S 1950 4825 875  300 
U 60E2CE7E
F0 "sheet60E2CE79" 50
F1 "INA.sch" 50
F2 "INPUT_+" I L 1950 4925 50 
F3 "INPUT_-" I L 1950 5050 50 
F4 "OUTPUT" O R 2825 4975 50 
$EndSheet
$Sheet
S 1950 5950 875  300 
U 60E5C973
F0 "sheet60E5C96B" 50
F1 "INA.sch" 50
F2 "INPUT_+" I L 1950 6050 50 
F3 "INPUT_-" I L 1950 6175 50 
F4 "OUTPUT" O R 2825 6100 50 
$EndSheet
Wire Wire Line
	1950 6050 1800 6050
Wire Wire Line
	1950 6175 1800 6175
Wire Wire Line
	2825 6100 2975 6100
Wire Wire Line
	1950 4925 1800 4925
Wire Wire Line
	1950 5050 1800 5050
Wire Wire Line
	2825 4975 2975 4975
Wire Wire Line
	1950 5475 1800 5475
Wire Wire Line
	1950 5600 1800 5600
Wire Wire Line
	2825 5525 2975 5525
Text GLabel 5350 5775 2    50   Input ~ 0
CC_MCP300
Text GLabel 5375 5275 2    50   Input ~ 0
RT_MSP300
Text GLabel 5375 4725 2    50   Input ~ 0
INJ_MSP300
Text GLabel 4250 5725 0    50   Input ~ 0
CC_MCP300_+
Text GLabel 4250 5850 0    50   Input ~ 0
CC_MCP300_-
Text GLabel 4225 5225 0    50   Input ~ 0
RT_MSP300_+
Text GLabel 4225 5350 0    50   Input ~ 0
RT_MSP300_-
Text GLabel 4250 4800 0    50   Input ~ 0
INJ_MSP300_-
Text GLabel 4250 4675 0    50   Input ~ 0
INJ_MSP300_+
$Sheet
S 4375 4575 875  300 
U 60C7013A
F0 "Sheet60C70139" 50
F1 "INA.sch" 50
F2 "INPUT_+" I L 4375 4675 50 
F3 "INPUT_-" I L 4375 4800 50 
F4 "OUTPUT" O R 5250 4725 50 
$EndSheet
$Sheet
S 4375 5625 875  300 
U 60E0ADF0
F0 "sheet60E0ADEB" 50
F1 "INA.sch" 50
F2 "INPUT_+" I L 4375 5725 50 
F3 "INPUT_-" I L 4375 5850 50 
F4 "OUTPUT" O R 5250 5775 50 
$EndSheet
$Sheet
S 4375 5125 875  300 
U 60E1043A
F0 "sheet60E10435" 50
F1 "INA.sch" 50
F2 "INPUT_+" I L 4375 5225 50 
F3 "INPUT_-" I L 4375 5350 50 
F4 "OUTPUT" O R 5250 5275 50 
$EndSheet
Wire Wire Line
	5375 5275 5250 5275
Wire Wire Line
	4375 5350 4225 5350
Wire Wire Line
	4225 5225 4375 5225
Wire Wire Line
	4375 4675 4250 4675
Wire Wire Line
	4250 4800 4375 4800
Wire Wire Line
	5250 4725 5375 4725
Wire Wire Line
	4375 5725 4250 5725
Wire Wire Line
	5350 5775 5250 5775
Wire Wire Line
	4250 5850 4375 5850
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 60EC289F
P 7775 2975
F 0 "J1" H 7855 3017 50  0000 L CNN
F 1 "Conn_01x03" H 7855 2926 50  0000 L CNN
F 2 "Connector_Molex:Molex_PicoBlade_53398-0371_1x03-1MP_P1.25mm_Vertical" H 7775 2975 50  0001 C CNN
F 3 "~" H 7775 2975 50  0001 C CNN
	1    7775 2975
	1    0    0    -1  
$EndComp
$EndSCHEMATC
