EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5368 4154 5825 4154
Wire Wire Line
	5368 3956 5368 4154
Wire Wire Line
	5443 3956 5368 3956
Wire Wire Line
	6318 4154 6025 4154
Wire Wire Line
	6318 3956 6318 4154
$Comp
L Device:R_Small R?
U 1 1 60E5E27E
P 5925 4154
AR Path="/60551091/60E5E27E" Ref="R?"  Part="1" 
AR Path="/6055E6DD/60E5E27E" Ref="R?"  Part="1" 
AR Path="/60E5E27E" Ref="R?"  Part="1" 
AR Path="/60E548FA/60E5E27E" Ref="R4"  Part="1" 
AR Path="/60EB75CA/60E5E27E" Ref="R8"  Part="1" 
AR Path="/60EDF494/60E5E27E" Ref="R6"  Part="1" 
F 0 "R6" H 5984 4200 50  0000 L CNN
F 1 "Rgain" H 5984 4109 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5925 4154 50  0001 C CNN
F 3 "~" H 5925 4154 50  0001 C CNN
	1    5925 4154
	0    1    1    0   
$EndComp
Text GLabel 4959 3656 0    50   Input ~ 0
Vref
Wire Wire Line
	4959 3656 4959 3731
Wire Wire Line
	5358 3731 5443 3731
Connection ~ 5358 3731
Wire Wire Line
	4959 3731 5358 3731
Wire Wire Line
	4959 3881 5443 3881
Wire Wire Line
	6835 3731 6944 3731
Connection ~ 6835 3731
Wire Wire Line
	6835 3881 6835 3731
Wire Wire Line
	6318 3731 6835 3731
Wire Wire Line
	6944 3731 6944 3740
Wire Wire Line
	6835 3881 6688 3881
Connection ~ 6403 3881
Wire Wire Line
	6403 3881 6488 3881
Wire Wire Line
	6318 3881 6403 3881
$Comp
L Device:R_Small R?
U 1 1 60E5E293
P 6588 3881
AR Path="/60551091/60E5E293" Ref="R?"  Part="1" 
AR Path="/6055E6DD/60E5E293" Ref="R?"  Part="1" 
AR Path="/60E5E293" Ref="R?"  Part="1" 
AR Path="/60E548FA/60E5E293" Ref="R5"  Part="1" 
AR Path="/60EB75CA/60E5E293" Ref="R9"  Part="1" 
AR Path="/60EDF494/60E5E293" Ref="R7"  Part="1" 
F 0 "R7" H 6647 3927 50  0000 L CNN
F 1 "10kOhm" H 6647 3836 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6588 3881 50  0001 C CNN
F 3 "~" H 6588 3881 50  0001 C CNN
	1    6588 3881
	0    1    1    0   
$EndComp
Wire Wire Line
	5358 3731 5358 3656
Wire Wire Line
	5443 3656 5358 3656
Wire Wire Line
	6403 3806 6403 3881
Wire Wire Line
	6318 3806 6403 3806
$Comp
L power:GND #PWR?
U 1 1 60E5E29D
P 6944 3740
AR Path="/60E5E29D" Ref="#PWR?"  Part="1" 
AR Path="/60AB28B1/60E5E29D" Ref="#PWR?"  Part="1" 
AR Path="/60E548FA/60E5E29D" Ref="#PWR018"  Part="1" 
AR Path="/60EB75CA/60E5E29D" Ref="#PWR024"  Part="1" 
AR Path="/60EDF494/60E5E29D" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 6944 3490 50  0001 C CNN
F 1 "GND" H 6949 3567 50  0000 C CNN
F 2 "" H 6944 3740 50  0001 C CNN
F 3 "" H 6944 3740 50  0001 C CNN
	1    6944 3740
	1    0    0    -1  
$EndComp
Wire Wire Line
	4959 3806 5443 3806
$Comp
L power:GND #PWR?
U 1 1 60E5E2A4
P 5089 3581
AR Path="/60E5E2A4" Ref="#PWR?"  Part="1" 
AR Path="/60AB28B1/60E5E2A4" Ref="#PWR?"  Part="1" 
AR Path="/60E548FA/60E5E2A4" Ref="#PWR016"  Part="1" 
AR Path="/60EB75CA/60E5E2A4" Ref="#PWR022"  Part="1" 
AR Path="/60EDF494/60E5E2A4" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 5089 3331 50  0001 C CNN
F 1 "GND" H 5094 3408 50  0000 C CNN
F 2 "" H 5089 3581 50  0001 C CNN
F 3 "" H 5089 3581 50  0001 C CNN
	1    5089 3581
	1    0    0    -1  
$EndComp
Wire Wire Line
	5089 3581 5443 3581
$Comp
L MotorControllerShield:INA125U U?
U 1 1 60E5E2AE
P 5818 3681
AR Path="/60E5E2AE" Ref="U?"  Part="1" 
AR Path="/60E548FA/60E5E2AE" Ref="U5"  Part="1" 
AR Path="/60EB75CA/60E5E2AE" Ref="U7"  Part="1" 
AR Path="/60EDF494/60E5E2AE" Ref="U6"  Part="1" 
F 0 "U6" H 5880 4171 50  0000 C CNN
F 1 "INA125U" H 5880 4080 50  0000 C CNN
F 2 "motorcontrollershield:SOP16_3.9_9.9" H 5593 3756 50  0001 C CNN
F 3 "" H 5593 3756 50  0001 C CNN
	1    5818 3681
	1    0    0    -1  
$EndComp
Wire Wire Line
	5296 3431 5296 3506
Connection ~ 5296 3431
Wire Wire Line
	5296 3305 5296 3431
Wire Wire Line
	5443 3506 5296 3506
Wire Wire Line
	5443 3431 5296 3431
$Comp
L power:+5V #PWR?
U 1 1 60E5E2B9
P 5296 3305
AR Path="/60E5E2B9" Ref="#PWR?"  Part="1" 
AR Path="/60AB28B1/60E5E2B9" Ref="#PWR?"  Part="1" 
AR Path="/60E548FA/60E5E2B9" Ref="#PWR017"  Part="1" 
AR Path="/60EB75CA/60E5E2B9" Ref="#PWR023"  Part="1" 
AR Path="/60EDF494/60E5E2B9" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 5296 3155 50  0001 C CNN
F 1 "+5V" H 5311 3478 50  0000 C CNN
F 2 "" H 5296 3305 50  0001 C CNN
F 3 "" H 5296 3305 50  0001 C CNN
	1    5296 3305
	1    0    0    -1  
$EndComp
Text HLabel 4959 3806 0    50   Input ~ 0
Signal+
Text HLabel 4959 3881 0    50   Input ~ 0
Signal-
Wire Wire Line
	6403 3880 6403 3881
Wire Wire Line
	6403 4058 6681 4058
Wire Wire Line
	6403 3881 6403 4058
Text HLabel 6681 4058 2    50   Output ~ 0
SignalOut
$EndSCHEMATC
